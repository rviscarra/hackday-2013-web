from flask import *
from flask.ext.sqlalchemy import *
from comunicash import *

from datetime import datetime
import string
import random

from sqlalchemy.dialects import postgresql

app = Flask(__name__)

app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgres@localhost/comunicash'
db = SQLAlchemy(app)


#MODELS
class User(db.Model):
    uid = db.Column(db.Integer, primary_key=True)
    account = db.Column(db.String(5), unique=True)
    phone = db.Column(db.String(20))
    password = db.Column(db.String(100))
    is_authorized = db.Column(db.Boolean)
    transactions = db.relationship('Transaction', backref='sender', lazy='dynamic', foreign_keys='Transaction.suid')
    rtrans = db.relationship('Transaction', backref='receiver', lazy='dynamic', foreign_keys='Transaction.ruid')
    
    def __init__(self, account, phone, password, is_authorized):
        self.account = account
        self.phone = phone
        self.password = password
        self.is_authorized = is_authorized

    def __repr__(self):
        return '<User %r>' % self.account

class Transaction(db.Model):
    tid = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime)
    ttype = db.Column(db.String(10))
    amount = db.Column(db.Float)
    suid = db.Column(db.Integer, db.ForeignKey('user.uid'))
    ruid = db.Column(db.Integer, db.ForeignKey('user.uid'))

    def __init__(self, created_at, ttype, amount, sender, receiver):
        self.created_at = created_at
        self.ttype = ttype
        self.amount = amount
        self.sender = sender
        self.receiver = receiver

    def __repr__(self):
        return '<Transaction %r>' % self.tid

class Location(db.Model):
    lid  = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(50))
    lon  = db.Column(db.Numeric(19, 16))
    lat  = db.Column(db.Numeric(19, 16))
    tags = db.Column(postgresql.ARRAY(db.Text))

    def __init__(self, name, lon, lat, tags):
        self.name = name
        self.lon  = lon 
        self.lat  = lat 
        self.tags = tags

    def __repr__(self):
        return '<Location %r>' % self.lid



@app.route('/')
def index():
    if 'uid' in session:
        return redirect(url_for('dashboard'))   
    return render_template('index.html')

@app.route('/logout')
def logout():
    if 'uid' in session:
        session.pop('uid', None)
        return redirect(url_for('index'))
    else:   
        return redirect(url_for('index'))

@app.route('/about')
def about():
	return render_template('about.html')

@app.route('/signin', methods = ['GET','POST'])
def login():
    error = None
    
    if 'uid' in session:
        return redirect(url_for('dashboard'))       
    
    if request.method == 'POST':
        account = request.form['account'];
        password = request.form['password'];
        user = User.query.filter_by(account=account).first()

        if user is None:
            error = True
        else:
            if account == user.account and password == user.password:
                session['uid'] = user.uid
                return redirect(url_for('dashboard'))
            else:
                error = True
    
    return render_template('login.html', error=error)


@app.route('/dashboard', methods=['GET','POST'])
def dashboard():
    if 'uid' in session:
        uid = session['uid']
        user = User.query.filter_by(uid=uid).first()
        return render_template('dashboard.html', account = user.account, phone = user.phone)
    else:
        return redirect(url_for('login'))

@app.route('/transfer', methods=['GET','POST'])
def send():
    if 'uid' in session:
        uid = session['uid']
        user = User.query.filter_by(uid=uid).first()
        return render_template('transfer.html', phone=user.phone)
    else:
        return redirect(url_for('login'))

@app.route('/transactions')
def list_transactions():
    if 'uid' in session:
        uid = session['uid']
        user = User.query.filter_by(uid=uid).first()
        sent_trx = user.transactions.filter_by(ttype='OUT').all()
        received_trx = user.transactions.filter_by(ttype='IN').all()
        
        balance = get_balance_by_phone(user.phone)
        return render_template('balance.html', sent_trx = sent_trx, received_trx = received_trx, balance = balance)
    else:
        return redirect(url_for('login'))





#CREATE account
@app.route('/users/create', methods=['POST', 'GET'])
def create():
#GET BALANCE from account
    
    status = 'SUCCESS'
    
    try:
        #BY POST
        sender_phone = request.form['phone']
            
        #BY GET
        #sender_phone = request.args.get('phone','');
            
        sender = User.query.filter_by(phone=sender_phone).first()
        
        if sender is not None:
            data = sender.account;
        else:
            is_valid = True;
            while is_valid:
                new_account = generate_account()
                posible_user = User.query.filter_by(account=new_account).first()
                is_valid = posible_user
            
            new_user = User(new_account, sender_phone, None, False)
            
            db.session.add(new_user);
            db.session.commit();
            
            data = new_user.account
        
        
    except Exception as inst:
        
        status = 'FAIL'
        data = str(inst)
    
    return jsonify(status=status, data=data)

@app.route('/users/balance', methods=['GET'])
def balance():
    
    status = 'SUCCESS'
    
    try:
        #BY POST
        #sender_phone = request.form['phone']
            
        #BY GET
        sender_phone = request.args.get('phone','');
            
        sender = User.query.filter_by(phone=sender_phone).first()
        
        if sender is None:
            raise Exception('El numero no existe')
        
        balance = get_balance_by_phone(sender_phone)
        
        data = balance
        
    except Exception as inst:
        
        status = 'FAIL'
        data = str(inst)
    
    return jsonify(status=status, data=data)

#TRANSFER to another account
@app.route('/transactions/transfer', methods = ['GET', 'POST'])
def transfer():
    
    #Expected result
    status = 'SUCCESS'
    
    try:
        #BY POST
        sender_phone = request.form['phone']
        receiver_account = request.form['account']
        amount = request.form['amount']
        
        sender = User.query.filter_by(phone=sender_phone).first()
        receiver = User.query.filter_by(account=receiver_account).first()
        
        if sender is None:
            raise Exception('El numero no existe')
        
        if receiver is None:
            raise Exception('La cuenta no existe')
        
        if amount is None or float(amount) <= 0:
            raise Exception('Monto no permitido')
        
        if sender.uid == receiver.uid:
            raise Exception('Intento a transferir a la misma cuenta')
            
            
        #"Special" account for the app
        def_account = 'qwert'
        def_user = User.query.filter_by(account=def_account).first()
        
        now = datetime.now()
        
        if sender.is_authorized:
        #Withdraw
            sendtrx = Transaction(datetime.now(), 'OUT', amount, sender, def_user)
            recvtrx = Transaction(datetime.now(), 'IN', amount, def_user, sender)
            
        elif receiver.is_authorized:
        #Deposit
            sendtrx = Transaction(datetime.now(), 'OUT', amount, def_user, receiver)
            recvtrx = Transaction(datetime.now(), 'IN', amount, receiver, def_user)
            
        else:
        #Normal transfer
            sendtrx = Transaction(datetime.now(), 'OUT', amount, sender, receiver)
            recvtrx = Transaction(datetime.now(), 'IN', amount, receiver, sender)

        balance = get_balance_by_phone(sender.phone)

        if balance >= float(amount):
            db.session.add(sendtrx)
            db.session.add(recvtrx)
            db.session.commit()
        else:
            raise Exception('La cuenta no tiene saldo suficiente (saldo: $' + repr(balance) + ')')
        
        data = receiver.phone;
        
        
    except Exception as inst:
        
        status = 'FAIL'
        data = str(inst)
        
    return jsonify(status=status, data=data)

@app.route('/map', methods=['GET'])
def map():
    return render_template('map.html')

@app.route('/search/<term>', methods = ['GET'])
def search(term):
    locations = db.session.query(Location.name, Location.lat, Location.lon).filter(
        Location.tags.contains([term])
    ).all()
    return jsonify(locations = [{
        'lon': float(l.lon),
        'lat': float(l.lat),
        'name': l.name 
        }
        for l
        in locations
    ])

#Helper methods
def get_balance_by_phone(phone):
    
    sender = User.query.filter_by(phone=phone).first()
    
    #Calculate positive amounts
    positive = 0.0
    pcount = 0
    for trx in sender.transactions.filter_by(ttype='IN').all():
        positive = positive +  trx.amount
        pcount = pcount + 1
        #print 'Positive trxs count: ' + repr(pcount) + ' - Positive amount: $' + repr(positive)
        
    #Calculate negative amounts
    negative = 0.0
    ncount = 0
    for trx in sender.transactions.filter_by(ttype='OUT').all():
        negative = negative +  trx.amount
        ncount = ncount + 1
        #print 'Negative trxs count: ' + repr(ncount) + ' - Negative amount: $' + repr(negative)
        
    balance = positive - negative
    return balance
    

def generate_account(size=5, chars=string.ascii_lowercase):
    return ''.join(random.choice(chars) for x in range(size))


if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)